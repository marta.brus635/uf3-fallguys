using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Target;

    private int m_Index = 0;

    [SerializeField]
    private Vector3 m_Offset;

    [SerializeField]
    private float rotationSpeed;
    [SerializeField]
    private float yaw = 0, pitch = 20f;
    [SerializeField]
    private bool invertY = false;
    float distance = 0;

    private void Awake()
    {
        
    }

    void Update()
    {
        //distance - GetAxis perque vull allunyar amb el scroll cap enrere (negatiu)
        distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * .5f, 1.5f, 10f);
        yaw = yaw + Input.GetAxis("Mouse X") * rotationSpeed * Time.deltaTime;
        pitch = Mathf.Clamp(pitch + Input.GetAxis("Mouse Y") * rotationSpeed * Time.deltaTime * (invertY ? -1 : 1), -60f, 60f);

        //posar-se al punt que has de mirar
        transform.position = m_Target.transform.position
             + m_Offset.x * m_Target.transform.right
             + m_Offset.y * m_Target.transform.up
             + m_Offset.z * m_Target.transform.forward;
        //rotar
        transform.rotation = Quaternion.Euler(pitch, yaw, 0);
        //allunyar-se
        transform.position = transform.position - transform.forward * distance;
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float m_Speed;
    [SerializeField]
    private float m_RotationSpeed;
    [SerializeField]
    private float m_JumpSpeed;

    Vector3 movement = Vector3.zero;
    [SerializeField]
    float horizontal = 0;

    private Rigidbody m_Rigidbody;
    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        // no quieres saber que pasa si quitas esto martita
        movement = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
        {
            movement += transform.forward;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            movement -= transform.forward;
        }

        if (Input.GetKey(KeyCode.A))
        {
            movement -= transform.right;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            movement += transform.right;
        }

        if (Input.GetKey(KeyCode.Space))
        {
            movement += transform.up;
            //m_Rigidbody.AddForce(transform.position + transform.up * m_JumpSpeed);
        }
        //movement.Normalize();

        horizontal = horizontal + Input.GetAxis("Mouse X") * m_RotationSpeed * Time.deltaTime;
        transform.rotation = Quaternion.Euler(0, horizontal, 0);

    }

    private void FixedUpdate()
    {
        m_Rigidbody.MovePosition(transform.position + movement * m_Speed * Time.fixedDeltaTime);
    }
}

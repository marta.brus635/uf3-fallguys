using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RodilloController : MonoBehaviour
{
    [SerializeField]
    private float m_RotationSpeed;

    [SerializeField]
    private Vector3 rotation;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Rotate(rotation * (m_RotationSpeed * Time.fixedDeltaTime));
    }
}

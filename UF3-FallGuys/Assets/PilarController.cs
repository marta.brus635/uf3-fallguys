using UnityEngine;

public class PilarController : MonoBehaviour
{

    [SerializeField]
    int distance = 0;

    [SerializeField]
    private GameEvent trampa;

    [SerializeField]
    private LayerMask layerMask;

    // Update is called once per frame
    void Update()
    {
        RaycastHit raycastHit;
        Ray ray = new(transform.position, transform.right);
        Debug.DrawRay(transform.position, transform.right * distance, Color.cyan, 0.1f);
        if (Physics.Raycast(transform.position, transform.right, out raycastHit, distance, layerMask))
        {
            Debug.DrawLine(transform.position, raycastHit.point, Color.red, 0.1f);
            trampa.Raise();
        }
    }
}
